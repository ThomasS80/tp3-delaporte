﻿using System;

namespace TP3
{
    public class Station
    {
        public delegate void LavageStation(Voiture p_voiture);
        public static void Lavage(Voiture p_voiture, byte p_degre)
        {
            if (p_voiture.DegreSalissure >= p_degre)
            {
                p_voiture.DegreSalissure -= p_degre;
            }
            else
            {
                p_voiture.DegreSalissure = 0;
            }
        }
        public static void LavageFaible(Voiture p_voiture)
        {
            Lavage(p_voiture, 2);
        }
        public static void LavageNormal(Voiture p_voiture)
        {
            Lavage(p_voiture, 4);
        }
        public static void LavageIntense(Voiture p_voiture)
        {
            Lavage(p_voiture, 7);
        }
        public static void LavageDeluxe(Voiture p_voiture)
        {
            Lavage(p_voiture, 9);
        }
    }
}
