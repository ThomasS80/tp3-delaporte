﻿using System;

namespace TP3
{
    public class Color
    {
        private byte m_R;
        private byte m_V;
        private byte m_B;

        private byte m_MaxIntensity = 255;
        private static Color m_White = new Color(0, 0, 0);
        private static Color m_Black = new Color(255, 255, 255);
        private static Color m_Blue = new Color(0, 0, 255);

        public Color(byte p_R, byte p_V, byte p_B)
        {
            m_R = p_R;
            m_V = p_V;
            m_B = p_B;
        }

        //Bonus ajout d'accesseur get

        public byte r { get => m_R; }
        public byte v { get => m_V; }
        public byte b { get => m_B; }

        public byte MaxIntens { get => m_MaxIntensity; }

        public Color White { get => m_White; }
        public Color Black { get => m_Black; }
        public Color Blue { get => m_Blue; }
    }
}
