﻿using System;

namespace TP3
{
    public abstract class Vehicule
    {
        private String m_Type;
        private byte m_NbPassager;
        private int m_NbCapacity;
        public String Type { get => m_Type; }
        public byte NbPassager { get => m_NbPassager; }
        public int NbCapacite { get => m_NbCapacity; }
        public Vehicule(String p_type, byte p_nb_passenger, int p_nb_capacity)
        {
            m_Type = p_type;
            m_NbPassager = p_nb_passenger;
            m_NbCapacity = p_nb_capacity;
        }

        public Vehicule(String p_type, byte p_nb_passenger) : this(p_type, p_nb_passenger, 0) { }

        public Vehicule(String p_type) : this(p_type, 1, 0) { }

        public abstract String SeDeplacer();

        public virtual String GetInfosTechniques()
        {
            return String.Format("Type : {0}\nNbPassenger : {1}\nNbCapacity : {2}\n", m_Type, m_NbPassager, m_NbCapacity);
        }
    }
}
