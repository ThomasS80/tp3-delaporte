﻿using System;

namespace TP3
{
    public class Voiture : Vehicule
    {
        private byte m_degre_salissure;
        public byte DegreSalissure
        {
            get => m_degre_salissure;
            set
            {
                if (value <= 10)
                    m_degre_salissure = value;
            }
        }
        public Voiture(byte p_nb_passenger, int p_nb_capacity, byte p_degre_salisure) : base("Voiture", p_nb_passenger, p_nb_capacity)
        {
            m_degre_salissure = p_degre_salisure;
        }

        public Voiture(byte p_nb_passenger, int p_nb_capacity) : this(p_nb_passenger, p_nb_capacity, 0)
        {

        }
        public Voiture(byte p_nb_passenger) : this(p_nb_passenger, 0, 0)
        {
       
        }

        public override sealed String SeDeplacer()
        {
            return "AVANCE";
        }
    }

}
