﻿using System;

namespace TP3
{
    public class MaClass
    {
        public MaClass()
        {
            Console.WriteLine("Constructeur");
        }
        ~MaClass()
        {
            Console.WriteLine("Destructeur");
        }
    }
}
