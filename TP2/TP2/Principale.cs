﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3;

namespace TP3
{
    class Principale
    {
        static void Main(string[] args)
        {
            MaClass v_destruc = new MaClass();
            v_destruc = null;

            Voiture V = new Voiture(2,5,15);
            Console.WriteLine(V.GetInfosTechniques());
            Console.WriteLine(V.SeDeplacer());
            V.DegreSalissure = 10;
            Console.WriteLine(V.DegreSalissure);
            Station.LavageStation Lavage = Station.LavageFaible;
            Lavage(V);
            Console.WriteLine(V.DegreSalissure);
            Lavage = Station.LavageNormal;
            Lavage(V);
            Console.WriteLine(V.DegreSalissure);
            Lavage = Station.LavageIntense;
            Lavage(V);
            Console.WriteLine(V.DegreSalissure);
            V.DegreSalissure = 10;
            Console.WriteLine(V.DegreSalissure);
            Lavage = Station.LavageDeluxe;
            Lavage(V);
            Console.WriteLine(V.DegreSalissure);

        }
    }
}
